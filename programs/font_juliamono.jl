
@isdefined(FONT_DIR) || (const FONT_DIR = joinpath(ENV["HOME"],".local","share","fonts"))

const font_juliamono = Program(:font_juliamono,
                               commands=[Cmd(`rm -rf juliamono`, dir=FONT_DIR),
                                         Cmd(`git clone https://github.com/cormullion/juliamono`, dir=FONT_DIR),
                                         Cmd(`fc-cache`),
                                        ]
                              )

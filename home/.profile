# Manjaro defaults
export QT_QPA_PLATFORMTHEME="qt5ct"
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"

# duplicates variables set in .zshrc
export PATH=$HOME/sbin:$HOME/.cargo/bin:$PATH
export JULIA_BINDIR=/opt/julia/bin
export PATH=$PATH:$JULIA_BINDIR
export EDITOR=nvim
export VISUAL=nvim
export BROWSER=firefox

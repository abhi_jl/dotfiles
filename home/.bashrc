# contains essential exports that need to be available from everywhere
source "$HOME/.profile"

# for local stuff that can't be kept in a portable script
LOCALSOURCE="$HOME/.shlocal"

# loads a zsh script not attached to dotfiles repo to allow for local configs
if [[ -e $LOCALSOURCE ]]; then
    source $LOCALSOURCE
fi

# count the number of physical CPU cores
n_cpu_cores() {
    lscpu -b -p=Core,Socket | grep -v '^#' | sort -u | wc -l
}


# safer deleting
alias rm="rm -i"
# shortcut to full ls
alias ll="ls -lh"

# alias lsd, but we may not have it installed so check first
if [[ -x `which lsd` ]]; then
    alias ls="lsd -X"
    alias tree="lsd --tree"
fi

# some host machines may not recognize TERM=alacritty
alias ssh="TERM=xterm-256color ssh"

# really sick of typing this
alias dockerterm="docker run -it --rm"

# easier vpn management
alias vpn=protonvpn-cli

# easier to use dotfiles installer
alias install="$HOME/dotfiles/install"

# julia env
export JULIA_NUM_THREADS=`n_cpu_cores`
#export JULIA_BINDIR=/opt/julia/bin

if [[ -x `which bat` ]]; then
    alias cat=bat
    # handles man pages
    export MANPAGER="bat --style=grid"
    export MANLESS="bat --style=grid"
fi

if [[ -x `which zoxide` ]]; then
    eval "$(zoxide init zsh)"
    alias zr="zoxide remove"
    alias zri="zoxide remove -i"
    alias zq="zoxide query -ls"
fi

if [[ $0 =~ bash ]]; then
    if [[ -x `which starship` ]]; then
        eval "$(starship init bash)"
    fi
fi

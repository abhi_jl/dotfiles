# the Julia package template for my packages on GitLab
using PkgTemplates

using PkgTemplates: @with_kw_noshow, @plugin, Badge


@plugin struct GitLabCI2 <: PkgTemplates.FilePlugin
    file::String = PkgTemplates.default_file("gitlab-ci.yml")
    coverage::Bool = false
    extra_versions::Vector = PkgTemplates.DEFAULT_CI_VERSIONS_NO_NIGHTLY
end

PkgTemplates.badges(::Documenter{GitLabCI2}) = [Badge(
    "dev",
    "https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia",
    "https://{{{USER}}}.gitlab.io/{{{PKG}}}.jl/",
)]

PkgTemplates.gitignore(p::GitLabCI2) = p.coverage ? PkgTemplates.COVERAGE_GITIGNORE : String[]
PkgTemplates.source(p::GitLabCI2) = p.file
PkgTemplates.destination(::GitLabCI2) = ".gitlab-ci.yml"

PkgTemplates.make_canonical(::Type{GitLabCI2}) = PkgTemplates.gitlab_pages_url

PkgTemplates.badges(p::GitLabCI2) = [Badge(
    "build",
    "https://img.shields.io/gitlab/pipeline/{{{USER}}}/{{{PKG}}}.jl/master?style=for-the-badge",
    "https://gitlab.com/{{{USER}}}/{{{PKG}}}.jl/-/pipelines",
)]

function PkgTemplates.view(p::GitLabCI2, t::Template, pkg::AbstractString)
    Dict(
        "HAS_COVERAGE"=>p.coverage,
        "HAS_DOCUMENTER"=>PkgTemplates.hasplugin(t, Documenter{GitLabCI2}),
        "HOST"=>t.host,
        "PKG"=>pkg,
        "USER"=>t.user,
        "VERSION"=>PkgTemplates.format_version(t.julia),
        "VERSIONS"=>PkgTemplates.collect_versions(t, p.extra_versions),
    )
end

badge_order() = DataType[Documenter{GitLabCI2}; GitLabCI2]

t = Template(;
             user="ExpandingMan",
             dir="~/dev/",
             julia=v"1.6",
             host="gitlab.com",
             plugins=[
                Git(;manifest=false, ssh=true),
                Tests(),
                License(name="MIT"),
                Readme(badge_order=badge_order()),
                GitLabCI2(extra_versions=[]),
                Documenter{GitLabCI2}(),
                !TagBot,
                !CompatHelper,
             ]
)
